<?php 
	if (!DEFINED('ACCESS')) {
		exit ("You are on other page");
	}
	function Connect () {
		try {
			// $connection = new PDO("mysql:host=localhost;dbname=Blogs",'root','');
			$connection = new PDO("mysql:host=176.9.122.83;dbname=Blogs",'Stas0238','testtest');
			$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			// echo "Connected successfully";
		}
		catch(PDOException $e) {
			echo "Connection failed: " . $e->getMessage();
		}
		return $connection;
	}
	function files_include ($var1) {
		include "tmp/".$var1.".php";
	}
	function ExcreteBlogs ($sql = "SELECT * FROM `blogs` LIMIT 3") { // show all blogs
		$connection = Connect ();
		$result = $connection->query($sql);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) { 
			$id = $row['id']; 
			$sql_comments = "SELECT * FROM `blogs_comments` WHERE  `num_post`=$id";
			$count_comments = $connection->query($sql_comments);
			$count = "";
			while ($comments = $count_comments->fetch(PDO::FETCH_ASSOC)) {
			$count++;
			}
			 ?>
			<article class="post">
				<div class="row">
					<div class="date"><span><?php echo date('j M', strtotime($row['date'])); ?></span></div>
					<div class="col-md-10 col-sm-10 col-xs-10 blog-text">
						<div class="blog-attributes">
							<ul class="list-unstyled">
								<li><span>cat : </span><a href="#"><?php echo $row['category']; ?></a></li>
								<li class="blog-likes"><a href="index.php?action=addlike&id=<?php echo $row['id']; ?>"><i class="fa fa-heart" aria-hidden="true"></i></a><span><?php echo $row['likes']; ?> Likes</span></li>
								<li class="blog-comments"><a href="index.php"><i class="material-icons">mode_comment</i></a><span><?php echo $row['comments']; ?> Comments</span></li>
							</ul>
						</div>
						<div class="blog-content">
							<h2><a href="<?php echo 'index.php?action=more&id='. $row['id']; ?>"><?php echo $row['title']; ?></a></h2>
							<p><?php echo $row['description']; ?></p>
						<a class="readMore" href=<?php echo 'index.php?action=more&id='. $row['id']; ?>>Read More</a>
						</div>
					</div>
				</div>
			</article>
		<?php }
	}
	function ExcreteOneBlog () { // show one blog 
		$connection = Connect ();
		$id = $GLOBALS['id'] = $_GET['id'];
		$sql = "SELECT * FROM `blogs` WHERE `id`=$id";
		$result = $connection->query($sql); 
		$row = $result->fetch(PDO::FETCH_ASSOC); ?>
		<div class="container">
			<article class="one-blog clearfix">
				<div class="date"><span><?php echo date('j M', strtotime($row['date'])); ?></span></div>
				<div class="col-md-10 col-sm-10 col-xs-10 blog-text">
					<div class="blog-attributes">
						<ul class="list-unstyled">
							<li> <span>cat :</span><a href="#"><?php echo $row['category']; ?></a></li>
							<li class="blog-likes"><a href="index.php?action=addlike&id=<?php echo $id; ?>"><i class="fa fa-heart" aria-hidden="true"></i></a><span><?php echo $row['likes']; ?> Likes</span></li>
							<li class="blog-comments"><a href="index.php"><i class="material-icons">mode_comment</i><span><?php echo $row['comments']; ?> Comments</span></a></li>
						</ul>
					</div>
					<div class="blog-content">
						<h2><?php echo $row['title']; ?></h2>
						<p><?php echo $row['text']; ?></p>
						<img class='blog-img' src="<?php echo $row['img']; ?>" alt=''>
						<div class="tags"><span>Tags</span> <a href="#">holiday , </a> <a href="#">diary , </a> <a href="#">bestmomments</a></div>
					</div>
				</div>
			</article>
			<div class="blog-footer clearfix"><div class="count-comments"><?php echo $row['comments']; ?> comments</div><div class="slide"><a class="next-stories" href="#">Next  Stories ></a></div></div>
	<?php }
	function ExcreteAdminBlogs ($sql = "SELECT * FROM `blogs`") { // shows all blogs on admin page
		$connection = Connect ();
		$result = $connection->query($sql);?>
		<div class="container">
			<div class="row">
			<?php while ($row = $result->fetch(PDO::FETCH_ASSOC)) { 
				$id = $row['id']; 
				$sql_comments = "SELECT * FROM `blogs_comments` WHERE  `num_post`=$id";
				$count_comments = $connection->query($sql_comments);
				$count = "";
				while ($comments = $count_comments->fetch(PDO::FETCH_ASSOC)) {
				$count++;
				}
				 ?>
				<div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-2 col-sm-offset-2">
					<article class="post-admin table-responsive">
						<table class="table">
							<tr>
								<th>
									Id
								</th>
								<th>
									Title
								</th>
								<th>
									Date
								</th>
							</tr>
							<tr>
								<td>
									<?php echo $row['id']; ?>
								</td>
								<td>
									<?php echo $row['title']; ?>
								</td>
								<td>
									<?php echo date('j M', strtotime($row['date'])); ?>
								</td>
							</tr>
						</table>
						<div class="text-center">
							<a class="btn btn-warning pull-left" href="<?php echo 'index.php?action=more&id='. $row['id']; ?>">More</a>
							<a class="btn btn-primary" href="<?php echo '?action=editPost&id=' . $row['id']; ?>">Edit</a>
							<a class="btn btn-danger pull-right" href="<?php echo '?action=delPost&id=' . $row['id']; ?>">Delete</a>
						</div>
					</article>
				</div>
		<?php } ?>
			</div>
		</div>
	<?php }
	function EditAdminBlog () { // uses only admin to edit any post 
		$connection = Connect ();
		$id = $_GET['id'];
		$sql = "SELECT * FROM `blogs` WHERE `id` = $id";
		$post = $connection->query($sql);
		$row = $post->fetch(PDO::FETCH_ASSOC);
		$_SESSION['path_image'] = $row['img'];  ?>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2 col-xs-12">
					<form action="<?php echo '?action=updatePost&id=' . $id; ?>" method="POST" enctype="multipart/form-data">
						<div class="form-group">
							<label for="edit-title">Title</label><br/>
							<?php echo $_GLOBALS['error_title']; ?>
							<input type="text" min-height='30' name="edit_title" class="edit-post" id="edit-title" value="<?php echo $row['title']; ?>"><br/>
						</div>
						<div class="form-group">
							<label for="edit-date">Date</label><br/>
							<?php echo $_GLOBALS['error_date']; ?>
							<input type="date" name="edit_date" id="edit-date" class="edit-post" value="<?php echo $row['date']; ?>"><br/>
						</div>
						<div class="form-group">
							<label for="edit-category">Category</label><br/>
							<?php echo $_GLOBALS['error_category']; ?>
							<input type="text" min-height='30' name="edit_category" class="edit-post" id="edit-category" value="<?php echo $row['category']; ?> "><br/>
						</div>
						<div class="form-group">
							<label for="edit-descr">Description</label><br/>
							<?php echo $_GLOBALS['error_descr']; ?>
							<textarea name="edit_descr" class="edit-post" id="edit-descr" cols = '30' rows = '10'><?php echo $row['description']; ?></textarea><br/>
						</div>
						<div class="form-group">
							<label for="edit-image">Image</label><br/>
							<img src="<?php echo $row['img']; ?>" alt = ''>
							<input type="file" min-height='30' name="edit_image" class="edit-post" id="edit-image"><br/>
						</div>
						<div class="form-group">
							<label for="text">Text</label><br/>
							<?php echo $_GLOBALS['error_text']; ?>
							<textarea name="edit_text" class="edit-post" id="edit-text" cols='30' rows = '20'><?php echo $row['text']; ?></textarea><br/>
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-primary" name="Send" value="Update">
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php }
	function UpdateBlog () { // update post
		$connection = Connect ();
		$id = $_GET['id'];
		$title = addslashes($_POST['edit_title']);
		$category = addslashes($_POST['edit_category']);
		$description = addslashes($_POST['edit_descr']);
		$text = addslashes($_POST['edit_text']);
		$path_img = $_SESSION['path_img'];
		$sql = "UPDATE `blogs` SET `title` = '$title' , `category` = '$category' , `description` = '$description' , `img` = '$path_img', `text` = '$text' WHERE `id` = '$id'";
		$connection->query($sql);
	}
	function AddComment () { // post comment`s form to blog ?> 
			<div class="form-add-comment clearfix">
				<h3>Post A Comment</h3>
				<form class="form-horizontal" action="" method="POST">
				<?php if (!isset($_SESSION['login']) || empty($_SESSION['login']))  { ?>
					<div class="form-group">
					    <label for="add-comment-name" class="col-md-offset-2 col-sm-2 control-label">Use Your Real Name</label>
					    <div class="col-sm-2">
					   		<span><?php echo $GLOBALS['Add_name']; ?></span>
					    	<input type="text" class="form-control" id="add-comment-name" name='add_comment_name' placeholder="Name">
					</div>
					<div class="col-sm-2">
							<span><?php echo $GLOBALS['Add_email']; ?></span>
					    	<input type="email" class="form-control" id="add-comment-email" name="add_comment_email" placeholder="Email">
					    </div>
					    <label for="add-comment-email" class="col-sm-2 control-label">Email Will not published</label>
					</div>
					<?php }
					else { echo "<h2>You are logined in</h2>"; } ?>
					<div class="form-group">
					  	<label for="text-comment" class="col-md-offset-2 col-sm-2 control-label">Write a good comment</label>
					    <div class="col-sm-4">
					    	<span><?php echo $GLOBALS['Add_comment']; ?></span>
					    	<input type="hidden" name="date" value="<?php echo date('Y-m-n'); ?>">
					    	<input type="hidden" name="time" value="<?php echo date('H:i:s'); ?>">
					    	<textarea type="text" class="form-control" id="text-comment" name="text_comment" placeholder="Comments" cols="10" rows="8" ></textarea>
					    </div>
				  	</div>
			    	<div class="col-md-offset-4 col-sm-4">
			    		<input type="submit" class="send-comment" name="add_comment" value="POST">
				    </div>
			    </form>
			</div>
	<?php }
	function ShowComments () { // show comments and may show only 3 comments 
		$connection = Connect ();
		$id = $_GET['id'];
		$sql_com = "SELECT * FROM `blogs_comments` WHERE `num_post` = $id";
		$comments = $connection->query($sql_com); 
		// $i = "";
		// while (($row_comments = $comments->fetch(PDO::FETCH_ASSOC)) & $i<3) { 
		while ($row_comments = $comments->fetch(PDO::FETCH_ASSOC)) :?>
				<div class="comment media">
				  <div class="media-left">
				    <a href="#">
				      <img class="media-object comment-avatar" src="<?php echo $row_comments['avatar']; ?>" alt="...">
				    </a>
				  </div>
				  <div class="media-body comment-body">
				    <h4 class="media-heading comment-heading"><?php echo $row_comments['login']; ?></h4>
				    <?php if ($_SESSION['key'] == '1') { ?>
				    	<a class="delComment" href="<?php echo "?action=delComment&id=" . $id . "&IdComm=" . $row_comments['id']; ?>"><i class="fa fa-times"></i></a>
				    <?php } ?>
				    <p class="comment-media"><?php echo $row_comments['comment']; ?></p>
				    <a class="comment-reply" href="#">Reply</a>
				  </div>
				</div>
		<?php endwhile;
		// $i++; 
		// if ($i == 3) { ?>
			<!-- </div>
			<div class="container">
				<form class="load-comments" action="index.php?action=moreComments" method="POST">
					<input type="submit" name="more_comments" id="more-comments" value="Show More Comments">
				</form>
			</div>
			-->
	<?php // } 
	}
	function DeleteComment () {
		$connection = Connect ();
		$id = $_GET['id'];
		$comment_id = $_GET['IdComm'];
		$sql = "DELETE FROM `blogs_comments` WHERE `id` = $comment_id ";
		$connection->query($sql);
		$sql_o = "SELECT * FROM `blogs` WHERE `id` = $id";
		$result = $connection->query($sql_o);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		$count = $row['comments'];
		$count = $count - 1;
		$sql_i = "UPDATE `blogs` SET `comments` = '$count' WHERE `id` = $id";
		$connection->query($sql_i);
	}
	function ShowMorePosts () { // shows blogs (count given from select)
		$count = $_POST['posts_count'];
		// switch ($count) {
		// 	case '3':
		// 		$GLOBALS['selected_3'] = "selected";
		// 		break;
		// 	case '9':
		// 		$GLOBALS['selected_9'] = "selected";
		// 		break;
		// 	case '12':
		// 		$GLOBALS['selected_12'] = "selected";
		// 		break;
		// 	case 'all':
		// 		$GLOBALS['selected'] = "selected";
		// 		break;
		// 	default:
		// 		$GLOBALS['selected'] = "";
		// }
		if ($count != "all") {
			ExcreteBlogs ($sql = "SELECT * FROM `blogs` LIMIT $count");
		}
		else {
			$connection = Connect ();
			$sql = "SELECT * FROM `blogs`";
			$connect = $connection->query($sql);
			$select = $connect->rowCount();
			ExcreteBlogs ($sql = "SELECT * FROM `blogs`");
		}
	}
	function InsertComment ($login, $id, $email, $comment) { // write down the comment to blog 
		$connection = Connect ();
		$sql = "INSERT INTO `blogs_comments`(`login`, `id`, `email`, `comment`) VALUES ('$login', '$id', '$email', '$comment')";
		$ins = $connection->quety($sql);
	}

	function UpdateLikes () { // renew likes 
		$connection = Connect ();
		$id = $_GET['id'];;
		$sql = "SELECT * FROM `blogs` WHERE `id`='$id'";
		$count = $connection->query($sql);
		$sum = $count->fetch(PDO::FETCH_ASSOC);
		$num = $sum['likes'];
		$num++;
		$login = $_SESSION['login'];
		$sql = "UPDATE `blogs` SET `likes`= '$num' WHERE `id`=$id";
		$sql_add_liker = "INSERT INTO `blogs_likers`(`num_post`, `login`) VALUES ('$id', '$login')";
		$connection->query($sql);
		$connection->query($sql_add_liker);
	}
	function DelUserLike () { // delete users likes 
		$connection = Connect();
		$id = $_GET['id'];
		$sql = "DELETE FROM `blogs_likers` WHERE `num_post` = '$id'";
		$connection->query($sql);
		$sql_i = "SELECT * FROM `blogs` WHERE `id` = '$id'";
		$row = $connection->query($sql_i);
		$result = $row->fetch(PDO::FETCH_ASSOC);
		$likes = $result['likes'];
		$likes = $likes - 1;
		$sql_u = "UPDATE `blogs` SET `likes` = '$likes' WHERE `id` = '$id'";
		$connection->query($sql_u);
	}
	function UpdateComments () { // renew comments count 
		$connection = Connect ();
		$sql = "SELECT * FROM `blogs`";
		$result = $connection->query($sql);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) { 
			$id = $row['id']; 
			$sql_comments = "SELECT * FROM `blogs_comments` WHERE  `num_post`=$id";
			$count_comments = $connection->query($sql_comments);
			$count = "";
			while ($comments = $count_comments->fetch(PDO::FETCH_ASSOC)) {
			$count++;
			}
			$update_comments = "UPDATE `blogs` SET `comments`='$count' WHERE `id`=$id";
			$connection->query($update_comments);
		}
	}
	function UpdateCommentsAvatars () { // update user`s avatars in comments after change user avatar
		$login = $_SESSION['login'];
		$avatar = $_SESSION['avatar'];
		$connection = Connect ();
		$sql = "UPDATE `blogs_comments` SET `avatar`= '$avatar' WHERE `login`= '$login'";
		$sql_i = "UPDATE `registration_users` SET `avatar` = '$avatar' WHERE `login` = '$login'";
		$connection->query($sql);
		$connection->query($sql_i);
	}
	function CutAvatar () { // cut user`s avatar and set on default 
		CheckDelUserAvatar ();
		$login = $_SESSION['login'];
		$avatar = CheckUserAvatar ();
		$connection = Connect ();
		$sql = "UPDATE `blogs_comments` SET `avatar` = '$avatar' WHERE `login`='$login'";
		$sql_o = "UPDATE `registration_users` SET `avatar` = '$avatar' WHERE `login`='$login'";
		$connection->query($sql);
		$connection->query($sql_o);
	}
	function DeleteUser () { // delete account of user
		UpdateUsersComments ();
		$connection = Connect ();
		$login = $_SESSION['login'];
		$sql = "DELETE FROM `blogs_comments` WHERE `login`= '$login'";
		$sql_i = "DELETE FROM `registration_users` WHERE `login` = '$login'";
		$connection->query($sql);
		$connection->query($sql_i);
		CheckDelUserAvatar ();
		unset($_SESSION['login']);
	}
	function UpdateUsersComments () { // update count comment for blog 
		$connection = Connect ();
		$login = $_SESSION['login'];
		$sql = "SELECT * FROM `blogs_comments` WHERE `login`='$login'";
		$result = $connection->query($sql);
		while($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$id = $row['num_post'];
			$sql_count = "SELECT * FROM `blogs` WHERE `id` = '$id'";
			$count = $connection->query($sql_count);
			$sum = $count->fetch(PDO::FETCH_ASSOC);
			// print_r($sum);
			// echo "<br/>";
			$count_comments = $sum['comments'] - 1;
			// echo $count_comments;
			// $sql_del = "UPDATE `blogs` SET `comments`='$count_comments' WHERE `id`='$id'";
			// $connection->query($sql_del);
		}
	}
	function DelPost () {
		$connection = Connect ();
		$id = $_GET['id'];
		$sql = "DELETE FROM `blogs` WHERE `id` = $id";
		$connection->query($sql);
		$sql_o = "DELETE FROM `blogs_likers` WHERE `num_post` = $id";
		$connection->query($sql_o);
		$sql_i = "DELETE FROM `blogs_comments` WHERE `num_post` = $id";
		$connection->query($sql_i);
	}
	function MyAccount () {
		$connection = Connect ();
		$login = $_SESSION['login'];
		$sql = "SELECT * FROM `registration_users` WHERE `login`= '$login'";
		$row = $connection -> query($sql);
		$result = $row->fetch(PDO::FETCH_ASSOC);
		$avatar = CheckUserAvatar (); ?>
			<div class="account-user row">
				<div class="account-avatar col-md-6 col-sm-6">
					<?php echo "<img src=" . $avatar . " alt=''>"; ?>
					<a class="del-avatar" href="?action=delAvatar" data-toggle="tooltip" data-placement="top" title="Cut image">
						<i class="fa fa-times" aria-hidden="true"></i>
					</a>
				</div>
				<div class="account-data col-ms-6 col-sm-6">
					<div class="user-login">
						Your login: <span><?php echo $result['login']; ?></span>
					</div>
					<div class="user-email">
						Your email: <span><?php echo $result['email']; ?></span>
					</div>
					<form class="form-avatar" action="?action=updateAvatar" method="POST" enctype="multipart/form-data">
						<input type="file" name="load" class="btn btn-primary" id="load-file" size="12"><br/>
						<input type="submit" class="btn btn-success" id="updateAv" value="Update Avatar">
					</form>
		<?php }
?>