<?php
	if (!DEFINED('ACCESS')) exit('Is not found');
	function Connect () {
		try {
			$connection = new PDO("mysql:host=localhost;dbname=todos_base",'root','');
			$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			// echo "Connected successfully";
		}
		catch(PDOException $e) {
			echo "Connection failed: " . $e->getMessage();
		}
	}
?>