<?php
	if (!DEFINED('ACCESS')) {
		exit ("You are on other page");
	}
	function CheckUserAvatar () {
		if ((isset($_SESSION['login'])) & (file_exists("images/" . $_SESSION['login']. "_avatar.jpg"))) {
			$_SESSION['avatar'] = "images/" . $_SESSION['login']. "_avatar.jpg";
		}
		else {
			$_SESSION['avatar'] = "images/avatar2.jpg";
		}
		return $_SESSION['avatar'];
		}
	function Check () { // check what file to include in case of action get 
		$avatar = CheckUserAvatar ();
		switch ($_GET['action']) {
			case 'addlike':
				files_include ('header');
				CheckLikes ();
				files_include ('content');
				break;
			case 'more':
				files_include ('header');
				if (isset ($_POST['add_comment'])) {
					CheckComments ();
					UpdateComments ();
				}
				files_include ('single');
				break;
			case 'user':
				files_include ('header');
				if ($_SESSION['login']) {
						files_include ('MyAccount');
					}
				else {
					files_include ('forms');
				}
				break;
			case 'delAvatar':
				CheckCutAvatar ();
				files_include ('header');
				files_include ("MyAccount");
				break;
			case 'regisrated':
				$check = CheckRegistration ();
				files_include ("header");
				if ($check) {
					files_include ("MyAccount");
				}
				else {
					files_include ('forms');
				}
				break;
			case 'enter':
				$set = CheckAutorization ();
				if ($set) {
					files_include ("header");
					files_include ('forms');
				}
				else {
					$avatar = CheckUserAvatar ();
					files_include ('header');
					files_include ("MyAccount");
				}
				break;
			case 'adminPage':
				files_include ('header');
				if (isset($_SESSION['login']) && ($_SESSION['key'] == '1')) {
					ExcreteAdminBlogs ();
				}
				break;
			case 'updateAvatar':
				$checkAvatar = CheckUploadAvatar ();
				$avatar = CheckUserAvatar ();
				if ($checkAvatar) {
					UpdateCommentsAvatars ();
				}
				files_include ('header');
				files_include ('MyAccount');
				break;
			case 'deleteAccount':
				if ($_SESSION['key'] != '1') {
					DeleteUser ();
					$avatar = CheckUserAvatar ();
					files_include ('header');
					files_include ('forms');
				}
				else {
					files_include ('header');
					files_include ('MyAccount');
				}
				break;
			case 'delPost':
				if ($_SESSION['key'] == '1') {
					DelPost ();
					files_include ('header');
					ExcreteAdminBlogs ();
				}
				break;
			case 'editPost':
				files_include('header');
				if ($_SESSION['key'] == '1') {
					files_include ('edit');
				}
				break;
			case 'updatePost':
				files_include('header');
				if ($_SESSION['key'] == '1') {
					$check = CheckUpdateBlog ();
					if (!$check) {
						UpdateBlog ();
						ExcreteAdminBlogs ();
					}
					else {
						files_include ('edit');
					}
				}
				break;
			case 'delComment':
				files_include ('header');
				if ($_SESSION['key'] == '1') {
					DeleteComment ();
					files_include ('edit');
				}
				break;
			case 'out':
				session_unset();
				$avatar = CheckUserAvatar ();
				files_include ('header');
				files_include ('forms');
				break;
			default:
				$avatar = CheckUserAvatar ();
				files_include ('header');
				files_include ('content');
		}
		files_include ('footer');
	}
	function ContentInclude () { // show blogs or if submit of sum blog is entered then it shows sum of those blogs 
		switch ($_GET['action']) {
			case 'showMe':
				ShowMorePosts ();
				break;
			default: 
				ExcreteBlogs ();
		}
	}
	function CheckComments () { // check wrote comment 
		if (isset($_SESSION['login']) && !empty($_SESSION['login'])) { 
			$login = $_SESSION['login'];
			$connection = Connect ();
			$sql = "SELECT * FROM `registration_users` WHERE `login` = '$login'";
			$row = $connection->query($sql);
			$result = $row->fetch(PDO::FETCH_ASSOC);
			$email = $result['email'];
		}
		else {
			$login = strip_tags($_POST['add_comment_name']);
			$email = $_POST['add_comment_email'];
		}
		$avatar = CheckUserAvatar ();
		$comment = strip_tags($_POST['text_comment']);
		$time = $_POST['time'];
		$date = $_POST['date'];
		$GLOBALS['Add_name'] = "";
		$GLOBALS['Add_email'] = "";
		$GLOBALS['Add_comment'] = "";
		$id = $GLOBALS['id'] = $_GET['id'];
		$check = false;
		if (empty($login)) {
			$GLOBALS['Add_name'] = "Write your name";
			$check = true;
		}
		if (empty($email) || !preg_match('/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/', $email)) {
			$GLOBALS['Add_email'] = "Write your email";
			$check = true;
		}
		if (empty($comment) || strlen($comment)>400) {
			$GLOBALS['Add_comment'] = "Write comment";
			$check = true;
		}
		if (!$check) {
			$connection = Connect ();
			$sql = "INSERT INTO `blogs_comments`(`login`, `num_post`, `avatar`, `email`, `comment`, `date`, `time`) VALUES ('$login', '$id', '$avatar', '$email', '$comment', '$date', '$time')";
			$ins = $connection->query($sql);
		}
	}
	function CheckRegistration () { // check registration form 
		$login = strip_tags($_POST['login_reg']);
		$email = strip_tags($_POST['email_reg']);
		$password = $_POST['password_reg'];
		$GLOBALS['add_login_reg'] = $GLOBALS['add_email_reg'] = "";
		$check = false;
		if (empty($login) || strlen($login)>20) {
			$GLOBALS['add_login_reg'] = "<span>Write your login</span><br/>";
			$check = true;
		}
		if (empty($email) || !preg_match('/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/', $email)) {
			$GLOBALS['add_email_reg'] = "<span>Write your email</span><br/>";
			$check = true;
		}
		if (empty($password) || strlen($password) > 20) {
			$GLOBALS['add_password_reg'] = "<span>Write your password</span><br/>";
			$check = true;
		}
		if (!$check) {
			$connection = Connect ();
			$sql = "SELECT * FROM `registration_users` WHERE `login`= '$login' OR `email`= '$email'";
			$user = $connection->query($sql);
			$row = $user->fetch(PDO::FETCH_ASSOC);
			if ($row['login'] == $login) {$GLOBALS['add_login_reg'] = "<span>This login is used</span><br/>";}
			if ($row['email'] == $email) {$GLOBALS['add_email_reg'] = "<span>This email is used</span><br/>";}
			if (empty($row)) {
				$sql = "INSERT INTO `registration_users`(`login`, `email`, `password`) VALUES ('$login', '$email', '$password')";
				$connection->query($sql);
				$default_avatar = 'images/avatar2.png';
				$folder =  'images/';//директория в которую будет загружен файл
				$new_name = $_POST['login_reg'] . '_avatar' . '.'. "png";
				$uploadedFile = $folder.$new_name;
				move_uploaded_file($default_avatar, $uploadedFile);
				$_SESSION['login'] = $login;
				$_SESSION['key'] = $row['key'];
				$send = true;
				return $send;
			}
		}
	}
	function CheckAutorization () { // check autorization users 
		$login = $_POST['au_login'];
		$password = $_POST['au_password'];
		$GLOBALS['add_au_login'] = $GLOBALS['add_au_password'] = "";
		$checking = false;
		$connection = Connect ();
		$sql = "SELECT * FROM `registration_users` WHERE `login`= '$login'";
		$row = $connection->query($sql);
		$row_assoc = $row->fetch(PDO::FETCH_ASSOC);
		if (!$row_assoc) {
			$GLOBALS['add_au_login'] = "<span>Write correct login</span><br/>";
			$GLOBALS['add_au_password'] = "<span>Write correct password</span><br/>";
			$checking = true;
		}
		if ($login != $row_assoc['login']) {
			$GLOBALS['add_au_login'] = "<span>Write correct login</span><br/>";
			$checking = true;
		}
		if ($password != $row_assoc['password']) {
			$GLOBALS['add_au_password'] = "<span>Write correct password</span><br/>";
			$checking = true;
		}
		if (!$checking) {
			$_SESSION['login'] = $login;
			$_SESSION['key'] = $row_assoc['key'];
		}
		return $checking;
	}
	function CheckLikes () { // check remove likes in table if like was already there or add likes in other case
			$connection = Connect ();
			$login = $_SESSION['login'];
			$id = $_GET['id'];
			$sql = "SELECT * FROM `blogs_likers` WHERE `num_post` = '$id'";
			$row = $connection->query($sql);
			$result = $row->fetch(PDO::FETCH_ASSOC);
			if (!$result && !empty($_SESSION['login'])) {
				UpdateLikes ();
			}
			elseif (empty($_SESSION['login'])) {
				echo "Log in first please !";
			}
			else {
				DelUserLike ();
			}
		}
	function CheckDelUserAvatar () { // check if user had avatar then after delete account delete avatar from server
		if (file_exists("images/" . $_SESSION['login']. "_avatar.jpg")) {
			echo "images/" . $_SESSION['login']. "_avatar.jpg";
			echo (unlink ("images/" . $_SESSION['login']. "_avatar.jpg"));
		}
	}
	function CheckUploadAvatar () { // check load user avatar on server 
   		// Проверяем загружен ли файл
   		$check = false;
		if ($_FILES['load']['error'] == 0) {
			// if ($_FILES['load']['size'] <= 100000) {
				$folder =  'images/';//директория в которую будет загружен файл
				$new_name = $_SESSION['login'] . '_avatar' . '.'. "jpg";
				$uploadedFile = $folder.$new_name;
				move_uploaded_file($_FILES['load']['tmp_name'], $uploadedFile);
				// echo "Your avatar is changed";
			// }
			$check = true;
		}
		// else {
		// 	echo "Error loading file";
		// }
		return $check;
	}
	function CheckCutAvatar () {
		if (isset($_SESSION['login']) && !empty($_SESSION['login']) && file_exists("images/" . $_SESSION['login']. "_avatar.jpg")) {
			CutAvatar ();
		}
		// else {
			// echo "You can`t cut your image";
		// }
	}
	function CheckUpdateBlog () {
		$check = false;
		$id = $_GET['id'];
		$image = $_FILES['edit_image']['type'];
		$image = explode('/', $image);
		$type_img = count($image) - 1;
		$format = $image[$type_img];
		if (empty($_POST['edit_title'])) {
			$_GLOBALS['error_title'] = "<span>Write any title</span><br/>";
			$check = true;
		}
		if (empty($_POST['edit_category'])) {
			$_GLOBALS['error_category'] = "<span>Write any category</span><br/>";
			$check = true;
		}
		if (empty($_POST['edit_descr'])) {
			$_GLOBALS['error_descr'] = "<span>Write blog description</span><br/>";
			$check = true;
		}
		if (empty($_POST['edit_date']) || preg_match("/[0-9]{2}-[0-9]{2}-[0-9]{4}/", $_POST['edit_date'])) {
			$_GLOBALS['error_date'] = "<span>Write correct date</span>";
			$check = true;
		}
		if (empty($_POST['edit_text'])) {
			$_GLOBALS['error_text'] = "<span>Write post`s text</span><br/>";
			$check = true;
		}
		if (!is_uploaded_file($_FILES['edit_image']['tmp_name'])) {
			$_SESSION['path_img'] = $_SESSION['path_image'];
		}
		else {
			// if ($_FILES['edit_image']['size'] <= 100000) {
			if ($format == 'jpeg' || $format == 'png') {
				$folder =  'images/';//директория в которую будет загружен файл
				$new_name = $_GET['id'] . 'post_image' . '.'. $format;
				$uploadedFile = $folder.$new_name;
				$_SESSION['path_img'] = $uploadedFile;
				move_uploaded_file($_FILES['edit_image']['tmp_name'], $uploadedFile);
				// echo "Your image is changed";
			}
			else {
				echo "Not correct type of file";
				$_SESSION['path_img'] = $_SESSION['path_image'];
				$check = true;
			}
			// }
		}
		return $check;
	}
	function CheckExcreteComments () {
		$connection = Connect ();
		$id = $_GET['id'];
		$sql_com = "SELECT * FROM `blogs_comments` WHERE `num_post` = $id";
		$comments = $connection->query($sql_com); 
		if ($row_assoc = $comments->fetch(PDO::FETCH_ASSOC)) {
			ShowComments ();
		}
		else {
			echo "<span class='empty-comments'>Not comments</span>";
		}
	}
?>