<?php if (!DEFINED('ACCESS')) exit('Is not found'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-theme.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="css/styles.css">
</head>
<body>
	<header>
		<div class="top-carousel">
			
		</div>
		<nav class="menu-border">
			<div class="container">
				<ul class="navbar-top clearfix">
					<li class="active">
						<a href="index.php">Home</a>
					</li>
					<li>
						<a href="#">About</a>
					</li>
					<li class="avatar"> 
						<a href="index.php?action=user"><img src="<?php $avatar = CheckUserAvatar (); echo $avatar; ?>" alt=""></a>
					</li>
					<li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Category<span class="caret"></span></a>
			          <ul class="dropdown-menu">
			            <li><a href="#">Action</a></li>
			            <li><a href="#">Another action</a></li>
			            <li><a href="#">Something else here</a></li>
			          </ul>
					</li>
					<li>
						<a href="#">Contact</a>
					</li>
				</ul>
			</div>
		</nav>
	</header>