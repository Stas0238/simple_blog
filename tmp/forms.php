<?php if (!DEFINED('ACCESS')) {
		exit ("You are on other page");
	}
?>
<div class="container group-of-forms">
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<form class="forms form-regist" action="index.php?action=regisrated" method="POST">
				<h2>Registration</h2>
				<div class="form-group">
					<label for="login-reg">Login</label><br/>
					<?php echo $GLOBALS['add_login_reg']; ?>
				    <div class="input-group">
				      <div class="input-group-addon form-input-icon  user-icon"><i class="fa fa-user-o" aria-hidden="true"></i></div>
				      <input type="text" name="login_reg" id="login-reg" value="<?php echo $_POST['login_reg']; ?>"><br/>
				    </div>
				</div>
				<div class="form-group">
					<label for="email-reg">Email</label><br/>
					<?php echo $GLOBALS['add_email_reg']; ?>
				    <div class="input-group">
				      <div class="input-group-addon form-input-icon user-mail"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
				      <input type="email" name="email_reg" id="email-reg" value="<?php echo $_POST['email_reg']; ?>"><br/>
				    </div>
				</div>
				<div class="form-group">
					<label for="password-reg">Password</label><br/>
					<?php echo $GLOBALS['add_password_reg']; ?>
				    <div class="input-group">
				      <div class="input-group-addon form-input-icon user-password"><i class="fa fa-unlock-alt" aria-hidden="true"></i></i></div>
				      <input type="password" name="password_reg" id="password-reg"><br/>
				    </div>
				</div>
				<input class="submit-form btn btn-primary" type="submit" name="Send" value="Registration">
			</form>
		</div>
		<div class="col-md-3 col-sm-3 col-md-offset-3 col-sm-offset-3 col-xs-12">
			<form class="forms form-autoriz" action="index.php?action=enter" method="POST">
				<h2>Sign in</h2>
				<div class="form-group">
					<label for="au_login">Login</label><br/>
					<?php echo $GLOBALS['add_au_login']; ?>
				    <div class="input-group">
				      <div class="input-group-addon form-input-icon user-icon"><i class="fa fa-user-o" aria-hidden="true"></i></div>
				      <input type="text" name="au_login" id="au-login" value="<?php echo $_POST['au_login']; ?>"><br/>
				    </div>
				</div>
				<div class="form-group">
					<label for="au-password">Password</label><br/>
					<?php echo $GLOBALS['add_au_password']; ?>
				    <div class="input-group">
				      <div class="input-group-addon form-input-icon user-password"><i class="fa fa-unlock-alt" aria-hidden="true"></i></i></div>
				      <input type="password" name="au_password" id="au-password"><br/>
				    </div>
				</div>
				<input class="submit-form" type="submit" name="sign" value="Sign in">
			</form>
		</div>
	</div>
</div>