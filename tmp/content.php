<?php if (!DEFINED('ACCESS')) {
		exit ("You are on other page");
	}
	$num = $_POST['posts_count']; ?>
<div class="container">
	<div class="welcome">
		<span class="bottom-lines">Hello, <?php echo $_SESSION['login']; ?> Welcome to my Blog!</span>
	</div>
</div>
<div class="container">
	<div class="posts">
		<?php ContentInclude (); ?>
	</div>
	<div class="load-footer">
		<form action="?action=showMe" method="POST">
			<input class="load-posts" type="submit" value="Show">
			<select name="posts_count" id="count_posts">
				<option <?php if (isset ($num) && $num== '3') echo "selected"; ?> value="3">3</option>
				<option <?php if (isset ($num) && $num== '9') echo "selected"; ?> label="9" value="9">9</option>
				<option <?php if (isset ($num) && $num== '12') echo "selected"; ?> value="12">12</option>
				<option <?php if (isset ($num) && $num== 'all') echo "selected"; ?> value="all">all</option>
			</select>
			<span>posts</span>
		</form>
	</div>
</div>